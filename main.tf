
resource "aws_instance" "TestIn" {
  ami    = "ami-04d29b6f966df1537"
  instance_type = "t2.micro"

  tags = {
    Name  =  "TestIn"
    Purpose ="Terraform Test"
    Environment = "Dev"
  }
}

